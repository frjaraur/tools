#!/bin/bash
 
Usage(){
        echo "$0 [-h for help] [-d for server to ask for CA] [-p for port server to ask for CA (defaults to 443)]" 
            exit
        }

        while getopts "h:d:p:" arg; do
              case $arg in
                      h)
                        Usage
                       ;;
                      d)
                        DOMAIN_NAME=$OPTARG
                      ;;
                      p)
                        PORT=$OPTARG
                        PORT=${PORT:=443}
                      ;;
                      *)
                        Usage
                      ;;
              esac
        done

       [ ! -n "${DOMAIN_NAME}" ] && Usage

       TMPFILE=/tmp/${DOMAIN_NAME}.crt

       OS="ubuntu"

       CERTFILE=/usr/local/share/ca-certificates/${DOMAIN_NAME}.crt

       [ -f /etc/redhat-release ] && OS="redhat" && CERTFILE=/etc/pki/ca-trust/source/anchors/${DOMAIN_NAME}.crt


       openssl s_client -connect ${DOMAIN_NAME}:${PORT} -showcerts </dev/null 2>/dev/null | openssl x509 -outform PEM | tee ${TMPFILE}

       sudo mv ${TMPFILE} ${CERTFILE}


       case ${OS} in
          redhat)
                sudo update-ca-trust
                sudo /bin/systemctl restart docker
          ;;

          *)
                sudo update-ca-certificates
                if [ -f /bin/systemctl ]
                then
                    sudo /bin/systemctl restart docker
                else    
                    sudo service docker restart
                fi
          ;;
       esac
